# Film Fan website

I built the website with a movie rental business in mind. 

## Instructions for running

1. Install [xampp](https://www.apachefriends.org/index.html) and [Node](https://nodejs.org/).
2. Clone the repository to the folder in `C:\xampp\htdocs`.
3. Run `npm start` in `C:\xampp\htdocs\front-end`.

## Accounts

| Email                 | Password  |
| --------------------- | --------- |
| acruz@filmfan.co.uk   | password1 |
| csayer@filmfan.co.uk  | password2 |
| gzentai@filmfan.co.uk | password3 |
| vstraub@filmfan.co.uk | password4 |
| bbishop@filmfan.co.uk | password5 |

## Assumptions

* I'm assuming the website is for a movie rental business.
* Title and Plot synopsis can be contained within a dialog which can be shown when the poster is clicked. This ensures the page does not get too long.
* The 2nd bonus task is to "Add simple authentication", I'm assuming this means that the authentication itself should be added. So I have made this from scratch in PHP. However, third-party authentication libraries and services could be beneficial as it would lessen the security burden of the project.
* The 2nd bonus task says "Add simple authentication for any user (or all users)" so I haven't added a registration page. For this reason, I have modified the `sqldump.sql` file to have hardcoded, hashed and salted passwords.
* The 2nd bonus task says "...**only** authenticated user could **view** or edit their favourite movies". I'm assuming the client still wants the public to see the chosen movies. Similar to a "staff picks" section that you might find on similar sites.

## Next steps

* One important change would be to add a password change feature. This is important as a leaked or cracked password could lead to the site being defaced.
* Work with the client to implement a registration system so that new staff members can be added to the system. As opposed to just having default accounts and passwords that are manually added to the database.

## Languages

 - [PHP](https://www.php.net/) is used for the backend logic. I used PHP because it is the backend language I am most familiar with. I am currently learning Node.
 - JavaScript is used for the front-end with React. See 'Libraries and Frameworks' below for more information.

## Libraries and Frameworks

 - [React](https://reactjs.org/) is used for the user interface. I chose to use React because it's a library I have used in the past and Lumina Learning uses it.
 - [Bootstrap](https://getbootstrap.com/) is used for some of the styling. I chose to use Bootstrap because it is extremely useful for simple styling of elements. It is also very useful because it provides some interactivity elements so I do not need to write JavaScript for something simple like opening a dialog box.
 - [React-Select](https://react-select.com/home) is used to provide the pillbox input seen on the `account` page. This is used to take in a selection of movies for display under the user's favourite movies. Usually, I would use [Select2](https://select2.org/) which I have used a number of times to provide similar inputs for assignments. However, this is intended for use with jQuery and is not optimized for React.

## Other tools

 - [create-react-app](https://create-react-app.dev/) simplifies creating a new React project.

## Sources

 - [OMDb API](https://www.omdbapi.com/) is used for access to IMDb data. 
 - [Video used for damaged film effect on navbar buttons.](https://youtu.be/J_MZb7qTenE)
 - [Montserrat](https://fonts.google.com/specimen/Montserrat) font used for body text. Google Fonts offer a large range of fonts [available free for personal/commercial use](https://developers.google.com/fonts/faq2#can_i_use_these_fonts_commercially_to_make_a_logo_for_print_media_for_broadcast_ebooks_apps_or_sewing_machines_and_apparel).
 - [Clipart used for film shape on navbar.](http://clipart-library.com/clipart/1529725.htm) 