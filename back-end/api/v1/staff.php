<?php
    
    header("Content-Type: application/json; charset=UTF-8");

    require_once('../../sql/conn.php');
    require_once('../../utils/auth.php');

    $requestMethod = $_SERVER['REQUEST_METHOD'];
    
    $comm = false;
    $data = array();

    switch ($requestMethod) {
        case 'GET':
            $id = $_GET['id'];

            if (isset($_GET['id'])) {
                $comm = $conn->prepare("SELECT id, firstName, lastName, email, favourite_movies FROM users WHERE id = ?");
                $comm->bind_param("i", $id);
            } else {
                $comm = $conn->prepare("SELECT id, firstName, lastName, email, favourite_movies FROM users");
            }
            break;
        case 'POST':
            if (!isLoggedIn()) {
                $data['error'] = "User is not logged in.";
                http_response_code(401);
                echo json_encode($data);
                // Exit early or the code will continue executing.
                exit();
            }

            $comm = $conn->prepare("UPDATE users SET firstName=?, lastName=?, email=?, favourite_movies=? WHERE id = ?");
            $comm->bind_param(
                "ssssi",
                $_POST['firstName'],
                $_POST['lastName'],
                $_POST['email'],
                $_POST['favourite_movies'],
                $_SESSION['userId']
            );
            break;
    }

    if ($comm == false) {
        $data['error'] = "Syntax error";
        http_response_code(500);
    } else {
        $comm->execute();
        $result = $comm->get_result();

        if ($result == false && $requestMethod !== 'POST') {
            $data['error'] = "Query error";
            http_response_code(500);
        } else {
            if ($requestMethod == 'GET') {
                $data['staff'] = $result->fetch_all(MYSQLI_ASSOC);
            } else if ($requestMethod == 'POST') {
                $data['affected_rows'] = $conn->affected_rows;
            }
        }
    }

    echo json_encode($data);

    $conn->close();
    exit();
?>