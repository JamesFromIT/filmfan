<?php
    require_once('sql/conn.php');
    require_once('utils/auth.php');

    $data = array();

    $comm = $conn->prepare("SELECT * FROM users WHERE email=?");

    if ($comm == false) {
        echo "Failed";
        exit();
    }

    $comm->bind_param("s", $_POST['email']);

    $comm->execute();
    $result = $comm->get_result();

    if ($result->num_rows !== 1) {
        $data['error'] = "Login was incorrect";
    }

    $row = $result->fetch_assoc();

    $passwordIsCorrect = password_verify($_POST['password'], $row['pwd']);

    if ($passwordIsCorrect) {
        session_start();
        $_SESSION['userId'] = $row['id'];
        
        $data['user']['id'] = $row['id'];
        $data['user']['firstName'] = $row['firstName'];
        $data['user']['lastName'] = $row['lastName'];
        $data['user']['email'] = $row['email'];
        $data['user']['favourite_movies'] = $row['favourite_movies'];
    } else {
        $data['error'] = "Login was incorrect.";
    }

    echo json_encode($data);

    $conn->close();
    exit();

?>