<?php
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Origin: http://localhost:3000');

    session_start();
    session_unset();
    session_destroy();

    exit();
?>