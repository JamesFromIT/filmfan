<?php
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Origin: http://localhost:3000');

    // Disable PHP error_reporting which echo's out warnings which break JSON pages.
    ini_set('display_errors', 0);

    try {
        $conn = new mysqli("localhost", "root", "", "filmfan");
    } catch (Exception $e) {
        http_response_code(503);
        exit();
    }

?>