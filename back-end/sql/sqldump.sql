CREATE TABLE users (
  id int(11) NOT NULL,
  firstName varchar(255) default NULL,
  lastName varchar(255) default NULL,
  email varchar(255) default NULL,
  pwd varchar(255) default NULL,
  favourite_movies varchar(255) default NULL,
  CONSTRAINT pk_users PRIMARY KEY(id)
);

-- Hard-coded hashed and salted passwords are used here purely for demonstration purposes.
INSERT INTO users (id,firstName,lastName,email,pwd,favourite_movies)
    VALUES
        (1,'Anona','Cruz','acruz@filmfan.co.uk','$2y$10$1OAft4Sz0h7rBtysBV0ItO2XPt5QIkWrd1XDtdY3rKloREs1D8UyW','tt0848228,tt4154756,tt2395427,tt4154796'),
        (2,'Camilla','Sayer','csayer@filmfan.co.uk','$2y$10$jmPF3vfnVVufNydsjoLe.Omw0Dms1TIpLF3J.wfwStufHMcXBfyqK','tt4154756,tt10515848,tt0120575'),
        (3,'Ganesh','Zentai','gzentai@filmfan.co.uk','$2y$10$cx53tMv9xK0L0CgNHzh/EOTXGddac70DjBYYPOGMpRTDaxKCdzVqy','tt0287871,tt2975590,tt0103776,tt4116284,tt2313197'),
        (4,'Vivien','Straub','vstraub@filmfan.co.uk','$2y$10$p7zvB/6b/ZXDou3LS86VqeiUYIMCIw7TpntD/hfo0Gn7QAgUBjOlS','tt0926084,tt0417741'),
        (5,'Bernardita','Bishop','bbishop@filmfan.co.uk','$2y$10$nNTlBFLrPxO0cPSgH4B2gOHFZr3QoBR.C3wkpXzp97dyfFNtSVgb2','tt0389860');