import React from 'react';

import Select from 'react-select';

import Alert from './Alert';

import '../Styling/index.css';

class AccountDetails extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      options: [],
      selectedValues: [],
      user: {
        firstName: null,
        lastName: null,
        email: null,
        favourite_movies: ''
      }
    };

    this.onChange = this.onChange.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.setState({ user: this.props.user });

    const selectedIds = this.props.user.favourite_movies.split(',');

    const currentMovies = [];

    selectedIds.forEach((id) => {
      fetch(`https://www.omdbapi.com/?apikey=4d47fb53&i=${id}`)
        .then((data) => data.json())
        .then((data) => {
          if (data !== undefined) {
            currentMovies.push(
              { label: data.Title, value: data.imdbID }
            );
          }
        })
        .catch((err) => console.error(err));
    });

    this.setState({ options: currentMovies });
    this.setState({ selectedValues: currentMovies });
  }

  onChange(items) {
    this.setState({ selectedValues: items });

    const user = this.props.user;

    user.favourite_movies = items.map(item => item.value).join(',');

    this.setState({ user });
  }

  onInputChange(query) {
    fetch(`https://www.omdbapi.com/?apikey=4d47fb53&s=${query}`)
      .then((data) => data.json())
      .then((data) => data.Search)
      .then((data) => {
        if (data !== undefined) {
          const processed = data.map((movie) => (
            {
              label: movie.Title,
              value: movie.imdbID
            }
          ));

          this.setState({ options: processed });
        }
      })
      .catch((err) => console.error(err));
  }

  onSubmit(e) {
    e.preventDefault();

    const formData = new FormData(e.target);
    formData.append('favourite_movies', this.state.user.favourite_movies);

    const headers = {
      method: 'POST',
      credentials: 'include',
      body: formData
    }

    fetch(`http://localhost/filmfan/back-end/api/v1/staff.php`, headers)
      .then((data) => data.json())
      .then((data) => {
        if (data.error) {
          console.log(data.error);
          this.setState({status: data.error});
        } else if (data.affected_rows === 1) {
          this.setState({status: 'Success'});
        }
      })
      .catch((err) => console.error(err));
  }
 
  render() {
    return (
      <React.Fragment>
        <h2>Account details</h2>

        <button type="button" className="btn btn-danger mb-5" onClick={this.props.logout}>Logout</button>

        {this.state.status ? 
          <Alert status={this.state.status} />
        : null}
          
        <form onSubmit={this.onSubmit} method="POST">
          <label htmlFor="firstName">First name</label>
          <input type="text" id="firstName" className="form-control" name="firstName" defaultValue={this.props.user.firstName} required></input>

          <label htmlFor="lastName" className="mt-2">Last name</label>
          <input type="text" id="lastName" className="form-control" name="lastName" defaultValue={this.props.user.lastName} required></input>

          <label htmlFor="email" className="mt-2">Email</label>
          <input type="email" id="email" className="form-control" name="email" defaultValue={this.props.user.email} required></input>

          <label htmlFor="favourite-movies" className="mt-2">Favourite movies</label>
          <Select id="favourite-movies" options={this.state.options} value={this.state.selectedValues} onChange={this.onChange} onInputChange={this.onInputChange} placeholder="Click to activate" isMulti={true} />

          <input type="submit" value="Save" className="btn btn-primary mt-3"></input>
        </form>
      </React.Fragment>
    );
  }
}

export default AccountDetails;