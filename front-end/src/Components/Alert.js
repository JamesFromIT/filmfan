import React from 'react';

import '../Styling/index.css';

function Alert(props) {
  if (props.status === 'Success') {
    return (
      <div className="alert alert-success" role="alert">
          {props.status}
      </div>
    );
  } else {
    return (
      <div className="alert alert-danger" role="alert">
        {props.status}
      </div>
    );
  }
}

export default Alert;