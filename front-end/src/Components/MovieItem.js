import React from 'react';

import '../Styling/index.css';

class MovieItem extends React.Component {
  render() {
    return (
      <React.Fragment>
        <li className="movie-item">
          <img src={this.props.movie.Poster} title={this.props.movie.Title} alt={this.props.movie.Title} className="movie-thumb" data-toggle="modal" data-target={"#movieItem" + this.props.index}></img>

          <div className="modal fade" role="dialog" id={"movieItem" + this.props.index} tabIndex="-1" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">More information</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <section className="movie-metadata">
                    <h3>{this.props.movie.Title}</h3>
                    <small><b className="all-caps">{this.props.movie.Type}</b> | {this.props.movie.Rated} | {this.props.movie.Runtime} | {this.props.movie.Genre} | {this.props.movie.Released}</small>
                    <p>{this.props.movie.Plot}</p>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </li>
      </React.Fragment>
    );
  }
}

export default MovieItem;