import React from 'react';

import MovieItem from './MovieItem';

import '../Styling/index.css';

class MovieList extends React.Component {
  state = {
    movies: {}
  }

  componentDidMount() {
    const movies = this.props.staff.favourite_movies.split(',');

    for (let i = 0; i < movies.length; i++) {
      fetch(`http://www.omdbapi.com/?apikey=4d47fb53&plot=full&i=${movies[i]}`)
        .then((data) => data.json())
        .then((data) => {
          const currentState = {...this.state.movies};
          
          currentState[i] = data;

          this.setState({ movies: currentState });
        })
        .catch((err) => console.error(err));
    }
  }

  render() {
    return (
      <React.Fragment>
        <h3>{this.props.staff.firstName} {this.props.staff.lastName}'s Picks</h3>

        <ul className="unlist">
        {
          Object.keys(this.state.movies).map(key => <MovieItem key={key} index={key} movie={this.state.movies[key]} />)
        }
        </ul>
      </React.Fragment>
    );
  }
}

export default MovieList;