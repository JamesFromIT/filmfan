import React from 'react';

import HomeStill from '../Resources/Images/home-still.gif';
import HomeHover from '../Resources/Images/home-hover.gif';

import StaffPicksStill from '../Resources/Images/staff-picks-still.gif';
import StaffPicksHover from '../Resources/Images/staff-picks-hover.gif';

import LoginStill from '../Resources/Images/login-still.gif';
import LoginHover from '../Resources/Images/login-hover.gif';

import '../Styling/index.css';

function Navbar(props) {
  return (
    <nav>
      <a href="/" title="Home">
        <img className="static" src={HomeStill} alt="Home" />
        <img className="active" src={HomeHover} alt="Home" />
      </a>
      <a href="/staff-picks" title="Staff Picks">
        <img className="static" src={StaffPicksStill} alt="Staff Picks" />
        <img className="active" src={StaffPicksHover} alt="Staff Picks" />
      </a>
      <a href="/account" title="Login">
        <img className="static" src={LoginStill} alt="Login" />
        <img className="active" src={LoginHover} alt="Login" />
      </a>

      <section className="filler"></section>
    </nav>
  );
}

export default Navbar;