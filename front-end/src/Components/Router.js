import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from '../Pages/Home';
import StaffPicks from '../Pages/StaffPicks';
import Login from '../Pages/Login';

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/staff-picks" component={StaffPicks} />
      <Route exact path="/account" component={Login} />
    </Switch>
  </BrowserRouter>
);

export default Router;