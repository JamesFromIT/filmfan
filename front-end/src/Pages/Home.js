import React from 'react';

import Navbar from '../Components/Navbar';

import '../Styling/index.css';

class Home extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <main className="p-4">
          <h2>Home</h2>

          <p>Film Fan is a small, independent movie rental service based in Worcester.</p>

          <p>We have been renting out physical media since 1995 and offer a plethora of products and services to make every movie night special. From DVDs to popcorn, come on down and plan your movie night today!</p>
        </main>
      </React.Fragment>
    );
  }
}

export default Home;