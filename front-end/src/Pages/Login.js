import React from 'react';

import Navbar from '../Components/Navbar';

import '../Styling/index.css';
import AccountDetails from '../Components/AccountDetails';
import Alert from '../Components/Alert';

class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: {}
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.logout = this.logout.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();

    const headers = {
      method: 'POST',
      credentials: 'include',
      body: new FormData(e.target)
    }

    fetch(`http://localhost/filmfan/back-end/login-handler.php`, headers)
      .then((data) => data.json())
      .then((data) => {
        if (data.error) {
          this.setState({error: data.error});
        } else {
          this.setState({user: data.user});
        }
      })
      .catch((err) => console.error(err));
  }

  logout(e) {
    const headers = {
      credentials: 'include'
    }

    fetch(`http://localhost/filmfan/back-end/logout-handler.php`, headers)
      .then((data) => {
          this.setState({user: {}});
      })
      .catch((err) => console.error(err));
  }

  render() {
    if (this.state.user.id) {
      return (
        <React.Fragment>
          <Navbar />
          <main className="p-4">
            <AccountDetails user={this.state.user} logout={this.logout} />
          </main>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <Navbar />
          <main className="p-4">
            <h2>Login</h2>

            <form onSubmit={this.onSubmit} method="POST">
              {this.state.error ? 
                <Alert status={this.state.error} />
              : null}

              <div className="form-group">
                <label htmlFor="email">Email address</label>
                <input type="email" id="email" className="form-control" name="email"></input>
              </div>

              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="password" id="password" className="form-control" name="password"></input>
              </div>

              <input type="submit" className="btn btn-primary" value="Login"></input>
            </form>
          </main>
        </React.Fragment>
      );
    }
  }
}

export default Login;