import React from 'react';

import Navbar from '../Components/Navbar';
import MovieList from '../Components/MovieList';

import '../Styling/index.css';

class StaffPicks extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      staff: {}
    };
  }

  componentDidMount() {
    const headers = {
      credentials: 'include'
    }

    fetch(`http://localhost/filmfan/back-end/api/v1/staff.php`, headers)
      .then((data) => data.json())
      .then((data) => {
        this.setState({
          staff: data.staff
        });
      })
      .catch((err) => console.error(err));
  }

  render() {
    return (
      <React.Fragment>
        <Navbar />
        <main className="p-4">
          <h2>Staff Picks</h2>

          {
            Object.keys(this.state.staff).map((key) => <MovieList key={key} index={key} staff={this.state.staff[key]} />)
          }
        </main>
      </React.Fragment>
    );
  }
}

export default StaffPicks;