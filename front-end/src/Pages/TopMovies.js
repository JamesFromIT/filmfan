import React from 'react';

import Navbar from '../Components/Navbar';

import '../Styling/index.css';

function TopMovies(props) {
  return (
    <React.Fragment>
      <Navbar />
      <main className="p-4">
        <h2>Top Movies</h2>
      </main>
    </React.Fragment>
  );
}

export default TopMovies;